FROM ubuntu:18.04
FROM python:3
LABEL maintainer="peter.bourgonje@dfki.de"


RUN apt-get -y update && \
    apt-get upgrade -y && \
    apt-get install -y  software-properties-common git && \
    apt-get install -y python3-dev libxml2-dev libxslt1-dev git python-poppler poppler-utils libleptonica-dev &&\
    apt-get update -y



RUN mkdir /data
RUN git clone https://github.com/tesseract-ocr/tessdata_best /data/tessdata_best/tessdata
#RUN export TESSDATA_PREFIX=/data/tessdata_best/tessdata
RUN mkdir /data/temp

# apt install tesseract-ocr kept installing tesseract version 3 (and the above clones the 4 models, which are incompatible), so manually compiling tesseract 4 here:
RUN mkdir /broker
RUN mkdir /broker/tesseract
RUN git clone https://github.com/tesseract-ocr/tesseract /broker/tesseract
WORKDIR /broker/tesseract
RUN ./autogen.sh
RUN ./configure
RUN make
RUN make install
RUN ldconfig
RUN PATH=$PATH:/broker/tesseract/src/api/tesseract
RUN export PATH


## install prerequisites
RUN pip3 install Flask Flask-Cors pytesseract pdf2image
WORKDIR /broker

ADD OCR.py /broker
ADD flaskController.py /broker
ADD config.ini /broker

RUN export LC_ALL=C.UTF-8
RUN export LANG=C.UTF-8



EXPOSE 5000

ENTRYPOINT FLASK_APP=flaskController.py flask run --host=0.0.0.0
#CMD ["/bin/bash"]

