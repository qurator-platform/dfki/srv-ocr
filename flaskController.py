#!/usr/bin/python3
from flask import Flask, flash, request, redirect, url_for
from flask_cors import CORS
import os
import json
from werkzeug.utils import secure_filename
import configparser

# custom modules
import OCR
from ast import literal_eval

"""
unix requirements:
tesseract-ocr

pip requirements:
Flask
Flask-Cors
pytesseract
pdf2image


then to start run:
export FLASK_APP=main.py
export FLASK_DEBUG=1 (optional, to reload upon changes automatically)
python -m flask run


example calls:

curl -F 'pdffile=@temp_pdf_storage/machine_readable_single_column_2.pdf' -X POST localhost:5000/ocr

"""

############# reading configuration #############
config = configparser.ConfigParser()
config.read('config.ini') # TODO: kill app if this is not found
UPLOAD_FOLDER = config['storage']['upload_folder']
ALLOWED_EXTENSIONS = set(literal_eval(config['storage']['allowed_extensions']))




app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.secret_key = "super secret key"
CORS(app)


# comment/uncomment the desired services here
ocrr = OCR.TessReader()

@app.route('/welcome', methods=['GET'])
def dummy():
    return "Hello stranger, can you tell us where you've been?\nMore importantly, how ever did you come to be here?\n"




##################### OCR #####################
@app.route('/ocr', methods=['POST'])
def doOCR():

    if request.method == 'POST':
        if 'pdffile' not in request.files:
            flash('No file part')
            return redirect(request.url)
        if not request.args.get('lang'):
            return 'Please provide a language\n'
        if not request.args.get('lang') in ocrr.supportedLanguages:
            return 'Language "%s" not supported. Please select one of: %s' % (request.args.get('lang'), ocrr.supportedLanguages)
        
        _file = request.files['pdffile']
        if _file and os.path.splitext(_file.filename)[1][1:] in ALLOWED_EXTENSIONS: # os.path.splitext leaves dot in, hence the [1:]
            filename = secure_filename(_file.filename)
            _file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            #return 'INFO: File stored succesffully.\n'
            # may want to split file storing from analysis (like OCR), but leaving this code in for now to have an example
            txt = ocrr.pdf2txt(os.path.join(app.config['UPLOAD_FOLDER'], filename), request.args.get('lang'))
            return txt



    
if __name__ == '__main__':

    port = int(os.environ.get('PORT',5000))
    app.run(host='0.0.0.0', port=port, debug=True)
