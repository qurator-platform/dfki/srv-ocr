import pytesseract
import os
import configparser
from pdf2image import convert_from_path

class TessReader():

    def __init__ (self):
        # to get this to work, install tesseract (and make sure executable is in path) and download/configure tessdata dir
        pytesseract.pytesseract.tesseract_cmd = 'tesseract'
        config = configparser.ConfigParser()
        config.read('config.ini')
        self.tessdata_dir = config['ocr']['tessdata']
        self.tessdata_dir_config = '--tessdata-dir "%s"' % self.tessdata_dir
        self.supportedLanguages = set([os.path.splitext(os.path.basename(f))[0] for f in os.listdir(self.tessdata_dir)])

    def pdf2txt(self, pdf, lang):

        images = convert_from_path(pdf)
        pages = []
        for image in images:
            txt = pytesseract.image_to_string(image, lang=lang, config=self.tessdata_dir_config)
            pages.append(txt)

        return '\n\n'.join(pages)

